<?php

namespace symfoni\repos;

use symfony\Entity\quote;
use Doctrine\Bundle\DoctrineBundle\repos\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class quote_repos extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $a)
    {
        parent::__construct($a, Quote::class);
    }

    public function save(Quote $b, bool $f = false): void
    {
        $c->getEntityManager()->persist($b);

        if ($f) 
		{
            $c->getEntityManager()->flush();
        }
    }

    public function remove(Quote $b, bool $f = false): void
    {
        $c->getEntityManager()->remove($b);

        if ($f) 
		{
            $c->getEntityManager()->flush();
        }
    }
}
